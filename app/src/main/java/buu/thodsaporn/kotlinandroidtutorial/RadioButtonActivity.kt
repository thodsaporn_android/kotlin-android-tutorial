package buu.thodsaporn.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_radio_button.*

class RadioButtonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_button)
        radio_group.setOnCheckedChangeListener{ group, checkedId ->
            val radio = findViewById<RadioButton>(radio_group.checkedRadioButtonId)
            Toast.makeText(applicationContext, "On Checked Change: ${radio.text}", Toast.LENGTH_LONG).show()
        }
        button.setOnClickListener {
            val id = radio_group.checkedRadioButtonId
            if (id != -1) {
                val radio  = findViewById<RadioButton>(id)
                Toast.makeText(applicationContext, "On Button Click: " + "${radio.text}",
                    Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "On Button Click: " + "Nothing Selected",
                    Toast.LENGTH_LONG).show()
            }
        }
    }
    fun radio_button_cilck(view: View) {
        val radio = findViewById<RadioButton>(radio_group.checkedRadioButtonId)
        Toast.makeText(applicationContext, "On Click: ${radio.text}", Toast.LENGTH_LONG).show()
    }
}